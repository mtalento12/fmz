import arcpy, datetime


class fmzObs():
    def __init__(self,obs_conn,eng8_draft_conn):
    
        # obsolete date
        self.date_obs            = datetime.date.today().strftime('%Y%m%d')
       # arcpy.AddMessage(self.eng8_for_conn)
        """The source code of the tool."""
        self.path                = 'Y:\\gein\\work\\mt12\\fmz\\dev\\connections'
        self.in_path             = eng8_draft_conn
        arcpy.AddMessage(self.in_path)
        self.out_path            = obs_conn
        self.fmz100              = 'FMZ100'
        self.out_name            = 'FMZ100_'+self.date_obs
        arcpy.AddMessage(self.out_name)
        self.geometry_type       = 'POLYGON'
        self.source_data         = self.in_path + '\\' + self.fmz100
        arcpy.AddMessage(self.source_data)
        self.spatial_reference   = '4283' #arcpy.Describe(self.source_data).spatialReference
        self.config_keyword      = 'OBSOLETE1'

        self.field_mapping       = "FMZ \'FMZ\' true true false 10 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",FMZ,-1,-1;\
        FMZDIS \"FMZDIS\" true true false 8 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",FMZDIS,-1,-1;\
        FMZ_NO \"FMZ_NO\" true true false 30 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",FMZ_NO,-1,-1;\
        DESC1 \"DESC1\" true true false 254 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",DESC1,-1,-1;\
        DESC2 \"DESC2\" true true false 254 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",DESC2,-1,-1;\
        X_DESC \"X_DESC\" true true false 100 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",X_DESC,-1,-1;\
        FORESTS_FM \"FORESTS_FM\" true true false 8 Double 4 15 ,First,#,"+self.in_path+"\\"+self.fmz100+",FORESTS_FM,-1,-1;\
        ZDETFID \"ZDETFID\" true true false 8 Double 4 15 ,First,#,"+self.in_path+"\\"+self.fmz100+",ZDETFID,-1,-1;\
        DETAILNO \"DETAILNO\" true true false 17 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",DETAILNO,-1,-1;\
        LINEAR \"LINEAR\" true true false 18 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",LINEAR,-1,-1;\
        FFG_POMA \"FFG_POMA\" true true false 45 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",FFG_POMA,-1,-1;\
        FFG_SOMA \"FFG_SOMA\" true true false 15 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",FFG_SOMA,-1,-1;\
        FFG_MOMA \"FFG_MOMA\" true true false 45 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",FFG_MOMA,-1,-1;\
        FFG_LFP \"FFG_LFP\" true true false 31 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",FFG_LFP,-1,-1;\
        FFG_STQ \"FFG_STQ\" true true false 13 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",FFG_STQ,-1,-1;\
        TC_FIREZ \"TC_FIREZ\" true true false 17 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",TC_FIREZ,-1,-1;\
        TC_APIARY \"TC_APIARY\" true true false 17 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",TC_APIARY,-1,-1;\
        TC_CATCHM \"TC_CATCHM\" true true false 16 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",TC_CATCHM,-1,-1;\
        RF_SOS \"RF_SOS\" true true false 15 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",RF_SOS,-1,-1;\
        NE_BIOD \"NE_BIOD\" true true false 26 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",NE_BIOD,-1,-1;\
        EVC_ONLY \"EVC_ONLY\" true true false 22 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",EVC_ONLY,-1,-1;\
        EVC_OG \"EVC_OG\" true true false 30 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",EVC_OG,-1,-1;\
        GUIDELINE \"GUIDELINE\" true true false 49 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",GUIDELINE,-1,-1;\
        LOCAL_USE \"LOCAL_USE\" true true false 21 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",LOCAL_USE,-1,-1;\
        SMS_RES \"SMS_RES\" true true false 101 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",SMS_RES,-1,-1;\
        SMS_FLORA \"SMS_FLORA\" true true false 65 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",SMS_FLORA,-1,-1;\
        SMS_REC \"SMS_REC\" true true false 45 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",SMS_REC,-1,-1;\
        SMS_HISTO \"SMS_HISTO\" true true false 55 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",SMS_HISTO,-1,-1;\
        RDLSCAPE \"RDLSCAPE\" true true false 25 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",RDLSCAPE,-1,-1;\
        BLKCOMP \"BLKCOMP\" true true false 14 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",BLKCOMP,-1,-1;\
        FMZTYPE \"FMZTYPE\" true true false 11 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",FMZTYPE,-1,-1;\
        BLKTYP \"BLKTYP\" true true false 11 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",BLKTYP,-1,-1;\
        BLK \"BLK\" true true false 6 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",BLK,-1,-1;\
        FMZBLK \"FMZBLK\" true true false 10 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",FMZBLK,-1,-1;\
        Z100FID \"Z100FID\" true true false 8 Double 4 15 ,First,#,"+self.in_path+"\\"+self.fmz100+",Z100FID,-1,-1;\
        Z100UNIT \"Z100UNIT\" true true false 14 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",Z100UNIT,-1,-1;\
        DETAILPT \"DETAILPT\" true true false 12 Text 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",DETAILPT,-1,-1;\
        AREA_HA \"AREA_HA\" true true false 8 Double 4 15 ,First,#,"+self.in_path+"\\"+self.fmz100+",AREA_HA,-1,-1;\
        VERS_DATE \"VERS_DATE\" true true false 36 Date 0 0 ,First,#,"+self.in_path+"\\"+self.fmz100+",VERS_DATE,-1,-1;\
        LEGAL_SET \"LEGAL_SET\" true true false 2 Short 0 1 ,First,#,"+self.in_path+"\\"+self.fmz100+",LEGAL_SET,-1,-1;\
        SHAPE_AREA \"SHAPE_AREA\" false false true 0 Double 0 0 ,First,#;SHAPE_LEN \"SHAPE_LEN\" false false true 0 Double 0 0 ,First,#"
    
    def ex(self):
        arcpy.AddMessage('Creating Archive FMZ Feature Class')
        arcpy.AddMessage(self.out_path)
        arcpy.AddMessage(self.out_name)
        arcpy.AddMessage(self.spatial_reference)
        arcpy.CreateFeatureclass_management(self.out_path, self.out_name, self.geometry_type, self.source_data, "DISABLED", "DISABLED", self.spatial_reference, self.config_keyword, "0", "0", "0")
    
        self.input_fc = self.out_path + '\\' + self.out_name

        if arcpy.Exists(self.input_fc):
            arcpy.AddMessage('Inserting feature to ' + self.input_fc)
            arcpy.Append_management (self.source_data, self.input_fc, 'NO_TEST', self.field_mapping, "")
            # To do: Change Privilege to public
            # Enable all users to view the data
            arcpy.ChangePrivileges_management(self.out_path+'\\' + self.out_name,"PUBLIC","GRANT","")
        else:
            arcpy.AddMessage('Feature Class: ' +  self.input_fc + ' doesnt exist')

        return
