import arcpy


class load():
    def __init__(self, source_data, year, quarter):
        self.path           = r'D://Users//Martin.Talento//projects//fmz//fmz'
        self.target_sde     = r'{0}//{1}'.format(self.path,'geoproc.forests.sde')
        self.datenow        = "NOW()"
        self.year           = year
        self.quarter        = quarter
        self.fgdb           = 'PROCESSING'
        self.feat_dataset   = 'GDA'
        self.source_data    = source_data
        arcpy.AddMessage(self.source_data)
        
        
    def ex(self):
        if self.source_data == "fmz100":
            self.dataset           = self.source_data.upper()+"_"+self.quarter+"QTR"+self.year
            self.draft_data         = r'{0}//{1}//{2}//{3}//{4}//{5}'.format(self.path,self.year,self.quarter,self.fgdb,self.feat_dataset,self.dataset)
            arcpy.AddMessage('source: '+self.target_sde)
            self.draft_sde           = r'{0}//{1}//{2}//{3}'.format(self.path ,self.year,self.quarter,"PROCESSING.gdb//GDA")
            fmz                 = r'{0}//{1}'.format(self.target_sde, "geoproc.forests.fmz100")
            #arcpy.AddMessage(fmz)
            self.field_maps       = 'FMZ \"FMZ\" true true false 10 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,FMZ,-1,-1;\
                FMZDIS \"FMZDIS\" true true false 8 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,FMZDIS,-1,-1;\
                FMZ_NO \"FMZ_NO\" true true false 30 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,FMZ_NO,-1,-1;\
                DESC1 \"DESC1\" true true false 254 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,DESC1,-1,-1;\
                DESC2 \"DESC2\" true true false 254 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,DESC2,-1,-1;\
                X_DESC \"X_DESC\" true true false 100 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,X_DESC,-1,-1;\
                FORESTS_FM \"FORESTS_FM\" true true false 8 Double 4 15 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,FORESTS_FM,-1,-1;\
                ZDETFID \"ZDETFID\" true true false 8 Double 4 15 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,ZDETFID,-1,-1;\
                DETAILNO \"DETAILNO\" true true false 17 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,DETAILNO,-1,-1;\
                LINEAR \"LINEAR\" true true false 18 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,LINEAR,-1,-1;\
                FFG_POMA \"FFG_POMA\" true true false 45 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,FFG_POMA,-1,-1;\
                FFG_SOMA \"FFG_SOMA\" true true false 15 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,FFG_SOMA,-1,-1;\
                FFG_MOMA \"FFG_MOMA\" true true false 45 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,FFG_MOMA,-1,-1;\
                FFG_LFP \"FFG_LFP\" true true false 31 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,FFG_LFP,-1,-1;\
                FFG_STQ \"FFG_STQ\" true true false 13 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,FFG_STQ,-1,-1;\
                TC_FIREZ \"TC_FIREZ\" true true false 17 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,TC_FIREZ,-1,-1;\
                TC_APIARY \"TC_APIARY\" true true false 17 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,TC_APIARY,-1,-1;\
                TC_CATCHM \"TC_CATCHM\" true true false 16 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,TC_CATCHM,-1,-1;\
                RF_SOS \"RF_SOS\" true true false 15 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,RF_SOS,-1,-1;\
                NE_BIOD \"NE_BIOD\" true true false 26 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,NE_BIOD,-1,-1;\
                EVC_ONLY \"EVC_ONLY\" true true false 22 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,EVC_ONLY,-1,-1;\
                EVC_OG \"EVC_OG\" true true false 30 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,EVC_OG,-1,-1;\
                GUIDELINE \"GUIDELINE\" true true false 49 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,GUIDELINE,-1,-1;\
                LOCAL_USE \"LOCAL_USE\" true true false 21 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,LOCAL_USE,-1,-1;\
                SMS_RES \"SMS_RES\" true true false 101 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,SMS_RES,-1,-1;\
                SMS_FLORA \"SMS_FLORA\" true true false 65 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,SMS_FLORA,-1,-1;\
                SMS_REC \"SMS_REC\" true true false 45 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,SMS_REC,-1,-1;\
                SMS_HISTO \"SMS_HISTO\" true true false 55 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,SMS_HISTO,-1,-1;\
                RDLSCAPE \"RDLSCAPE\" true true false 25 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,RDLSCAPE,-1,-1;\
                BLKCOMP \"BLKCOMP\" true true false 14 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,BLKCOMP,-1,-1;\
                FMZTYPE \"FMZTYPE\" true true false 11 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,FMZTYPE,-1,-1;\
                BLKTYP \"BLKTYP\" true true false 11 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,BLKTYP,-1,-1;\
                BLK \"BLK\" true true false 6 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,BLK,-1,-1;\
                FMZBLK \"FMZBLK\" true true false 10 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,FMZBLK,-1,-1;\
                Z100FID \"Z100FID\" true true false 8 Double 4 15 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,Z100FID,-1,-1;\
                Z100UNIT \"Z100UNIT\" true true false 14 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,Z100UNIT,-1,-1;\
                DETAILPT \"DETAILPT\" true true false 12 Text 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,DETAILPT,-1,-1;\
                AREA_HA \"AREA_HA\" true true false 8 Double 4 15 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,AREA_HA,-1,-1;\
                VERS_DATE \"VERS_DATE\" true true false 36 Date 0 0 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,VERS_DATE,-1,-1;\
                LEGAL_SET \"LEGAL_SET\" true true false 2 Short 0 1 ,First,#,'+self.target_sde+'//geoproc.forests.fmz100,LEGAL_SET,-1,-1;\
                SHAPE_AREA \"SHAPE_AREA\" false false true 0 Double 0 0 ,First,#;\
                SHAPE_LEN \"SHAPE_LEN\" false false true 0 Double 0 0 ,First,#'
            #arcpy.env.workspace = "U://BusinessData//fmz//engine8_geoproc.forests.sde"
            
            arcpy.AddMessage("draft data:   " + self.draft_data)
            arcpy.AddMessage("...Loading fmz100 into " + fmz)
            #arcpy.env.workspace = "U://BusinessData//fmz//vsdl_load_geoproc.forests.sde"
            #arcpy.TruncateTable_management(r'D:\\Users\\Martin.Talento\\projects\\fmz\\fmz\\geoproc.forests.sde\\geoproc.forests.fmz100')
            arcpy.Append_management(r'D://Users//Martin.Talento//projects//fmz//fmz//2019//1st//PROCESSING//GDA//FMZ100_1stQTR2019', r'D://Users//Martin.Talento//projects//fmz//fmz//geoproc.forests.sde//geoproc.forests.fmz100', "NO_TEST", self.field_maps, "")
            ora = arcpy.ArcSDESQLExecute(self.target_sde)
            sql = "UPDATE geoproc.forests.fmz100 set VERS_DATE = (select current_date from dual)"
            sql_return = ora.execute(sql)
            sql_return = [sql_return]

            #arcpy.CalculateField_management(fmz, "VERS_DATE", self.datenow, "VB", "")
            arcpy.AddSpatialIndex_management(fmz, 500)

        else:
            
            self.source_data      = self.draft_sde+"//DRAFTfmz_map100"
            self.fmzmap     = self.target_sde+"//geoproc.forests."
            arcpy.AddMessage(fmzmap)
            arcpy.AddMessage("...Loading FMZ_MAP into VSDLLOAD")
            arcpy.AddMessage("draft data: "+self.source_data)
            self.map_fields = 'FMZ_MAP \"FMZ_MAP\" true true false 10 Text 0 0 ,First,#,'+self.fmzmap+'//geoproc.forestsfmz_map100,FMZ_MAP,-1,-1;\
                FMZDIS \"FMZDIS\" true true false 8 Text 0 0 ,First,#,'+self.fmzmap+'//geoproc.forestsfmz_map100,FMZDIS,-1,-1;\
                MAP_NO \"MAP_NO\" true true false 30 Text 0 0 ,First,#,'+self.fmzmap+'//geoproc.forestsfmz_map100,MAP_NO,-1,-1;\
                X_DESC \"X_DESC\" true true false 100 Text 0 0 ,First,#,'+self.fmzmap+'//geoproc.forestsfmz_map100,X_DESC,-1,-1;\
                VERS_DATE \"VERS_DATE\" true true false 8 Text 0 0 ,First,#,'+self.fmzmap+'//geoproc.forestsfmz_map100,VERS_DATE,-1,-1;\
                AREA_HA \"AREA_HA\" true true false 30 Text 0 0 ,First,#,'+self.fmzmap+'//geoproc.forestsfmz_map100,AREA_HA,-1,-1'
        
            arcpy.Append_management(self.source_data, self.fmzmap, "NO_TEST", self.map_fields , "")
            
            arcpy.CalculateField_management(fmzmap, "VERS_DATE", datenow, "VB", "")
            arcpy.AddSpatialIndex_management(fmzmap, 500)

        #return