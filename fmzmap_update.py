import arcpy


class mapUpdate():
    def __init__(self,loc,conn):
        
        self.loc                 = loc
        self.target_sde          = self.loc+"\\geoproc_forests"
        self.ws_vg               = self.loc + "\\PROCESSING.gdb\\VG\\"
        self.ws_gda              = self.loc + "\\PROCESSING.gdb\\GDA\\"
        self.ws_map              = "\\fmz_map_tmp"
        # intermediate files
        self.sel_non_zone        = self.ws_vg + "sel_no_change"
        self.fmz_other           = self.ws_vg + "fmz_other_dis"
        self.sel_gmz             = self.ws_vg + "sel_merto_GMZ"
        self.sel_spz             = self.ws_vg + "sel_merto_spz"
        self.dis_spz             = self.ws_vg + "dis_spz"
        self.dis_gmz             = self.ws_vg + "dis_gmz"
        self.sel_smz             = self.ws_vg + "sel_merto_smz"
        self.dis_smz             = self.ws_vg + "dis_smz"
        self.dis_fields       = 'FMZ "FMZ" true true false 10 Text 0 0 ,First,#,' + self.fmz_other+',FMZ,-1,-1,' + self.dis_gmz+',FMZ,-1,-1,' + self.dis_smz + ',FMZ,-1,-1'+self.dis_spz+',FMZ,-1,-1;\
                            FMZDIS "FMZDIS" true true false 8 Text 0 0 ,First,#,'+self.fmz_other+',FMZDIS,-1,-1,'+self.dis_gmz+',FMZDIS,-1,-1,'+self.dis_smz+',FMZDIS,-1,-1 ,'+self.dis_spz+',FMZDIS,-1,-1; \
                            FMZ_NO "FMZ_NO" true true false 30 Text 0 0 ,First,#,'+self.fmz_other+', FMZ_NO,-1,-1,'+self.dis_gmz+',FMZ_NO,-1,-1,'+self.dis_smz+',FMZ_NO,-1,-1,'+self.dis_spz+',FMZ_NO,-1,-1;\
                            DESC1 "DESC1" true true false 254 Text 0 0 ,First,#,'+self.fmz_other+',DESC1,-1,-1,'+self.dis_gmz+',DESC1,-1,-1,'+self.dis_smz+',DESC1,-1,-1,'+self.dis_spz+',DESC1,-1,-1; \
                            DESC2 "DESC2" true true false 254 Text 0 0 ,First,#,'+self.fmz_other+',DESC2,-1,-1,'+self.dis_gmz+',DESC2,-1,-1,'+self.dis_smz+',DESC2,-1,-1'+self.dis_spz+',DESC2,-1,-1;\
                            X_DESC "X_DESC" true true false 100 Text 0 0 ,First,#,'+self.fmz_other+',X_DESC,-1,-1,'+self.dis_gmz+',X_DESC,-1,-1,'+self.dis_smz+',X_DESC,-1,-1,'+self.dis_spz+',X_DESC,-1,-1'


        # layers
        self.gmz_lyr                     = "gmz_lyr"
        self.smz_lyr                     = "sel_merto_smz_2"

        self.fmznew_vg                   = self.ws_vg +"dissolve"
        self.fmz_map                     = self.ws_vg +"fmzmap_vg"
        self.fmz_map_col                 = "FMZ_MAP"
        self.fmz_map_new                 = self.ws_gda+"FMZ_MAP100"

    def ex(self):

        # PROCESSING NON-ZONES
        # # Process: Select Non-Zone
        arcpy.AddMessage('PROCESSING NON-ZONES')
        arcpy.env.workspace = self.loc
        arcpy.AddMessage(self.loc)
        if arcpy.Exists(self.sel_non_zone):
            arcpy.AddMessage("..."+self.sel_non_zone+" exists. Deleting"+self.sel_non_zone)
            arcpy.Delete_management(self.sel_non_zone)
            arcpy.AddMessage("...Selecting CFP, GMZ and SMZ.")
            arcpy.Select_analysis(self.fmznew_vg, self.sel_non_zone, "\"FMZ\" NOT IN ( 'GMZ', 'CFP', 'SMZ','SPZ')")
            arcpy.CalculateField_management(self.sel_non_zone, "X_DESC",  "'No Zone'", "PYTHON", "")
            
        else:
            arcpy.AddMessage("..."+self.sel_non_zone+" inexistent. Deleting"+self.sel_non_zone)
            arcpy.AddMessage("...Selecting CFP, GMZ and SMZ.")
            arcpy.Select_analysis(self.fmznew_vg, self.sel_non_zone, "\"FMZ\" NOT IN ( 'GMZ', 'CFP', 'SMZ','SPZ')")
            arcpy.CalculateField_management(self.sel_non_zone, "X_DESC",  "'No Zone'", "PYTHON", "")



        # # Process: Dissolve (3)
        if arcpy.Exists(self.fmz_other):
            arcpy.AddMessage("..."+self.fmz_other+" exists. Deleting "+self.fmz_other)
            arcpy.Delete_management(self.fmz_other)
            arcpy.AddMessage("...Dissolving other")
            arcpy.Dissolve_management(self.sel_non_zone, self.fmz_other, "FMZ;FMZDIS;FMZ_NO;DESC1;DESC2;X_DESC", "", "SINGLE_PART", "DISSOLVE_LINES")
            
        else:
            arcpy.AddMessage("...Dissolving other")
            arcpy.Dissolve_management(self.sel_non_zone, self.fmz_other, "FMZ;FMZDIS;FMZ_NO;DESC1;DESC2;X_DESC", "", "SINGLE_PART", "DISSOLVE_LINES")
        
        # PROCESSING SPECIAL PROTECTION ZONES 
        # Process: Select (dissolve to spz)
        arcpy.AddMessage('PROCESSING SPZs')
        if arcpy.Exists(self.sel_spz):
            arcpy.AddMessage("..." + self.sel_spz + " exists. Deleting " + self.sel_spz)
            arcpy.Delete_management(self.sel_spz)
            arcpy.AddMessage("...Selecting SPZs")
            arcpy.Select_analysis(self.fmznew_vg, self.sel_spz, "\"FMZ\" = 'SPZ'")
            arcpy.CalculateField_management(self.sel_spz, "X_DESC",  "'Special Protection Zone'", "PYTHON", "")

        else:
            arcpy.AddMessage("...Selecting SPZs")
            arcpy.Select_analysis(self.fmznew_vg, self.sel_spz, "\"FMZ\" = 'SPZ'")
            arcpy.CalculateField_management(self.sel_spz, "X_DESC",  "'Special Protection Zone'", "PYTHON", "")

        # # Process: Calculate Field (2)
        arcpy.CalculateField_management(self.sel_spz, "FMZ",  "'SPZ'", "PYTHON", "")



        # Process: Dissolve (2)
        if arcpy.Exists(self.dis_spz):
            arcpy.AddMessage("..."+self.dis_spz+" exists. Deleting "+self.dis_spz)
            arcpy.Delete_management(self.dis_spz)
            arcpy.AddMessage("...Dissolving Codes into SPZs")
            arcpy.Dissolve_management(self.sel_spz, self.dis_spz, "FMZ;FMZDIS;FMZ_NO;DESC1;DESC2;X_DESC", "", "SINGLE_PART", "DISSOLVE_LINES")

        else:
            arcpy.AddMessage("...Dissolving Codes into SPZs")
            arcpy.Dissolve_management(self.sel_spz, self.dis_spz, "FMZ;FMZDIS;FMZ_NO;DESC1;DESC2;X_DESC", "", "SINGLE_PART", "DISSOLVE_LINES")

        # PROCESSING GMZs
        arcpy.AddMessage('PROCESSING GMZs')
        if arcpy.Exists(self.sel_gmz):
            arcpy.AddMessage("..." + self.sel_gmz + " exists. Deleting " + self.sel_gmz)
            arcpy.Delete_management(self.sel_gmz)
            arcpy.AddMessage("...Selecting GMZs")
            arcpy.Select_analysis(self.fmznew_vg, self.sel_gmz, "\"FMZ\" = 'CFP' AND \"FMZ_NO\" = ' ' OR \"FMZ\" = 'GMZ'")
            arcpy.CalculateField_management(self.sel_gmz, "X_DESC",  "'General Management Zone'", "PYTHON", "")

        else:
            arcpy.AddMessage("...Selecting GMZs")
            arcpy.Select_analysis(self.fmznew_vg, self.sel_gmz, "\"FMZ\" = 'CFP' AND \"FMZ_NO\" = ' ' OR \"FMZ\" = 'GMZ'")
            arcpy.CalculateField_management(self.sel_gmz, "X_DESC",  "'General Management Zone'", "PYTHON", "")

        # # Process: Calculate Field (2)
        arcpy.CalculateField_management(self.sel_gmz, "FMZ",  "'GMZ'", "PYTHON", "")



        # Process: Dissolve (2)
        if arcpy.Exists(self.dis_gmz):
            arcpy.AddMessage("..."+self.dis_gmz+" exists. Deleting "+self.dis_gmz)
            arcpy.Delete_management(self.dis_gmz)
            arcpy.AddMessage("...Dissolving Codes into GMZs")
            arcpy.Dissolve_management(self.sel_gmz, self.dis_gmz, "FMZ;FMZDIS;FMZ_NO;DESC1;DESC2;X_DESC", "", "SINGLE_PART", "DISSOLVE_LINES")

        else:
            arcpy.AddMessage("...Dissolving Codes into GMZs2")
            arcpy.Dissolve_management(self.sel_gmz, self.dis_gmz, "FMZ;FMZDIS;FMZ_NO;DESC1;DESC2;X_DESC", "", "SINGLE_PART", "DISSOLVE_LINES")

        # PROCESSING SMZ
        arcpy.AddMessage('PROCESSING SMZs')
        # Process: Select (Dissolve to SMZ)
        if arcpy.Exists(self.sel_smz):
            arcpy.AddMessage("...Dataset " + self.sel_smz + " exists. Deleting " + self.sel_smz)
            arcpy.Delete_management(self.sel_smz)
            arcpy.AddMessage("...Selecting SMZs")
            arcpy.Select_analysis(self.fmznew_vg, self.sel_smz, "\"FMZ\" = 'CFP' AND NOT \"FMZ_NO\" = ' ' OR \"FMZ\" = 'SMZ'")
            arcpy.CalculateField_management(self.sel_smz, "X_DESC",  "'Special Management Zone'", "PYTHON", "")

        else:
            arcpy.AddMessage("...Selecting SMZs2")
            arcpy.Select_analysis(self.fmznew_vg, self.sel_smz, "\"FMZ\" = 'CFP' AND NOT \"FMZ_NO\" = ' ' OR \"FMZ\" = 'SMZ'")
            arcpy.CalculateField_management(self.sel_smz, "X_DESC",  "'Special Management Zone'", "PYTHON", "")

        # Process: Calculate Field
        arcpy.CalculateField_management(self.sel_smz, "FMZ", "'SMZ'", "PYTHON", "")


        # Process: Dissolve
        if arcpy.Exists(self.dis_smz):
            arcpy.AddMessage("..."+self.dis_smz+" already exists. Deleting "+self.dis_smz)
            arcpy.Delete_management(self.dis_smz)
            arcpy.AddMessage("...Dissolving SMZs")
            arcpy.Dissolve_management(self.sel_smz, self.dis_smz, "FMZ;FMZDIS;FMZ_NO;DESC1;DESC2;X_DESC", "", "SINGLE_PART", "DISSOLVE_LINES")

        else:
            arcpy.AddMessage("...Dissolving SMZs2")
            arcpy.Dissolve_management(self.sel_smz, self.dis_smz, "FMZ;FMZDIS;FMZ_NO;DESC1;DESC2;X_DESC", "", "SINGLE_PART", "DISSOLVE_LINES")

        # Process: Merge
        
        if arcpy.Exists(self.fmz_map ):
            arcpy.AddMessage("..."+self.fmz_map +" exists. Deleting "+self.fmz_map )
            arcpy.Delete_management(self.fmz_map )
            arcpy.AddMessage("...Merging dissolved data")
            arcpy.Merge_management(self.fmz_other+';'+self.dis_gmz+';'+self.dis_smz+';'+self.dis_spz, self.fmz_map , self.dis_fields)
            #arcpy.AddMessage(t)
        else:
            arcpy.AddMessage("Map Inexistent. Merging dissolved data")
            arcpy.Merge_management(self.fmz_other+';'+self.dis_gmz+';'+self.dis_smz+';'+self.dis_spz, self.fmz_map , self.dis_fields)


        # Process: Add Field
        arcpy.AddField_management(self.fmz_map , "AREA_HA", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")

        # Process: Calculate Field (4)
        arcpy.CalculateField_management(self.fmz_map , "AREA_HA", "!SHAPE.AREA@HECTARES!", "PYTHON", "")
        arcpy.AlterField_management(self.fmz_map , "FMZ", self.fmz_map_col)
        
        # Process: Copy Features
        if arcpy.Exists(self.fmz_map_new):
            #arcpy.AddMessage("..."+self.fmz_map_new+" exists. Deleting "+self.fmz_map _new)
            arcpy.Delete_management(self.fmz_map_new)
            arcpy.AddMessage("...Creating new FMZ map version")
            arcpy.CopyFeatures_management(self.fmz_map , self.fmz_map_new, "", "0", "0", "0")
            #arcpy.AlterField_management(self.fmz_map_new, "FMZ", self.fmz_map_col)
            arcpy.AddField_management(self.fmz_map_new, "VERS_DATE", "DATE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
            #arcpy.CalculateField_management(self.fmz_map_new, "VERS_DATE", "NOW()", "VB", "")
            arcpy.AddSpatialIndex_management(self.fmz_map_new, 500)
            arcpy.FeatureClassToShapefile_conversion([self.fmz_map_new],self.loc)
            #arcpy.CopyFeatures_management(self.fmz_map,self.FMZ_OLD)

        else:
            arcpy.AddMessage("...Creating new FMZ map version")
            arcpy.CopyFeatures_management(self.fmz_map , self.fmz_map_new, "", "0", "0", "0")
            arcpy.AddField_management(self.fmz_map_new, "VERS_DATE", "DATE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
            #arcpy.AlterField_management(self.fmz_map_new, "FMZ", self.fmz_map_col)
            #arcpy.CalculateField_management(self.fmz_map_new, "VERS_DATE", "NOW()", "VB", "")
            arcpy.AddSpatialIndex_management(self.fmz_map_new, 500)
            arcpy.FeatureClassToShapefile_conversion([self.fmz_map_new],self.loc)



        return
