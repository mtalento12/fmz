#!/usr/bin/env python2.7
from os import environ
from traceback import format_exc
# 3 line dirty hack follows
from sys import path
import arcpy

################################################################################
# ref: https://docs.safe.com/fme/html/FME_REST/apidoc/v3/#!/transformations
#      https://playground.fmeserver.com/using-the-rest-api/jobs/
#      https://docs.python.org/2/howto/urllib2.html
# apidocs: http://is-prd-fme-00.aaa.depi.vic.gov.au/fmerest/apidoc/v3/#
# errCodes: https://docs.safe.com/fme/html/FME_REST/apidoc/v3/index.html
################################################################################

import urllib2
from urllib import urlencode
import json
from time import sleep
from datetime import datetime

################################################################################
class FmeParams(list):
  def __init__(self, listy):
    for el in listy: # list of name/value tuples
      self.append({"name":el[0],"value":el[1]})
  def asDict(self):
    return {"published params":self}
################################################################################
class FmeServer():
  def __init__(self, repo, wkspc):
    self.repo = repo
    self.wkspc = wkspc
    
    self.url = 'http://10.72.7.50'
    self.fmeAuthToken = "fmetoken token={0}".format('66f7766f961f872dd2898d9a31e5eadf2703c03f') # user:mt12. Expiration: 2026-07-30 00:00:00
   
  def __str__(self):
    return '%s-%s %s/%s' % ('True' if self.url<>None else 'False', self.repo, self.wkspc)
  ################################################################################
  def reqWithMeth(self, method, type, url, dataDict):
    if type=='json':
      headRecs = {"Content-Type":"application/json", "Accept":"application/json", "Authorization":self.fmeAuthToken}
      data = None if dataDict == None else json.dumps(dataDict).encode('utf-8')
    elif type=='urlenc':
      headRecs = {"Content-Type":"application/x-www-form-urlencoded", "Accept":"application/json", "Authorization":self.fmeAuthToken}
      data = None if dataDict == None else urlencode(dataDict)
    elif type=='octet':
      headRecs = {"Content-Type":"application/octet-stream","Content-Disposition":'attachment; filename="'+self.wkspc+'"', "Accept":"application/json", "Authorization":self.fmeAuthToken}
      data = dataDict
    elif type=='json/octet':
      headRecs = {"Content-Type":"application/json", "Accept":"application/octet-stream", "Authorization":self.fmeAuthToken}
      data = dataDict
    
    rspCode, rsp = None, None
    try:
      req = urllib2.Request(url, data, headRecs)
      req.get_method = lambda: method
      r = urllib2.urlopen(req)
      rspCode = r.getcode()
      resJson = r.read()
      resJson = resJson.decode('utf-8')
      if type=='json/octet':
        rsp = resJson
      elif resJson != "":
        rsp = json.loads(resJson)
    except urllib2.HTTPError, e: #the fme 404's have useful messages in them.
      rspCode = e.code
      rsp = json.loads(e.read()) #print "code:%s args:%s reason:%s read:%s, msg:%s" % (e.code, e.args, e.reason, e.read(), e.msg)
    return rspCode, rsp
  ################################################################################
  def execWkspc(self, fmeParams):
    arcpy.AddMessage("Submitting workspace job")
    #arcpy.AddMessage(fmeParams)
    #NB: following URL must have "v2" for the dev box. Replace when we are off FmeServer2019.
    url = '{0}/fmerest/v3/transformations/submit/{1}/{2}'.format(self.url, self.repo, self.wkspc)
    arcpy.AddMessage(url)
    rspCode, rsp = self.reqWithMeth('POST', 'json', url, fmeParams)
    if rspCode == 202:
      self.jobId = rsp['id']
      return True
    elif rspCode == 422:
      raise Exception("Unprocessable Entity - Please check that the input fmeParameters were correct.")
    else:
      raise Exception("Could not submitJob/get a jobId. HTTP Code:" + str(rspCode))
  
  def waitForJob(self):
    finished = False
    url = '{0}/fmerest/v3/transformations/jobs/id/{1}'.format(self.url, self.jobId)
    while not finished:
      rspCode, rsp = self.reqWithMeth('GET', 'json', url, None)
      finished = "result" in rsp.keys()
      if not finished:
        print ".",
        sleep(10)
    return True
  
  def getLog(self):
    url = '{0}/fmerest/v3/transformations/jobs/id/{1}/log'.format(self.url, self.jobId)
    arcpy.AddMessage("Getting logs...")
    rspCode, rsp = self.reqWithMeth('GET', 'json/octet', url, None)
    if rspCode == 200:
      #logPath = "%s/%s_job%s.log" % (self.argsDict['logDir'], self.wkspc, self.jobId)
      #return FmeLog(rsp)
      arcpy.AddMessage("response code 200")
      arcpy.AddMessage(rsp)
      
    else:
      raise Exception("Issue getting FME log file. HTTP Code: " + str(rspCode))
  ################################################################################
  def checkRepo(self):
    arcpy.AddMessage("Checking Repo...")
    url = '{0}/fmerest/v3/repositories'.format(self.url)
    rspCode, rsp = self.reqWithMeth('GET', 'json', url, None)
    if rspCode != 200: #Created
      raise Exception("Could not check connections. HTTPCode:" + rspCode)
    return True if self.repo in [item['name'] for item in rsp['items']] else False
  def createRepo(self):
    arcpy.AddMessage("Creating Repo...")
    url = '{0}/fmerest/v3/repositories'.format(self.url)
    dd = {"name":self.repo,"description":"autoCreated by VSDL.dataman scripts","sharable":False}
    rspCode, rsp = self.reqWithMeth('POST', 'urlenc', url, dd)
    if rspCode != 201: #Created
      raise Exception("Could not create connection. HTTPCode:" + str(rspCode))
    return True
  ################################################################################
  def checkWkspc(self):
    arcpy.AddMessage("Checking Workspace...")
    url = '{0}/fmerest/v3/repositories/{1}/items'.format(self.url, self.repo)
    rspCode, rsp = self.reqWithMeth('GET', 'json', url, None)
    return True if self.wkspc in [item['name'] for item in rsp['items']] else False
  def createWkspc(self):
    arcpy.AddMessage("Creating Workspace...")
    url = "{0}/fmerest/v3/repositories/{1}/items".format(self.url, self.repo)
    data = open("%s/%s" % (self.argsDict['progDir'], self.wkspc), 'r').read()
    rspCode, rsp = self.reqWithMeth('POST', 'octet', url, data)
    if rspCode != 201: #Created
      raise Exception("Could not create workspace. HTTPErrCode:" + str(rspCode))
    
    arcpy.AddMessage("Setting job submitter service on workspace")
    url = "{0}/fmerest/v3/repositories/{1}/items/{2}/services".format(self.url, self.repo, self.wkspc)
    dd = {"services":"fmejobsubmitter"}
    rspCode, rsp = self.reqWithMeth('PUT', 'urlenc', url, dd)
    if rspCode != 200:
      raise Exception("Could not set workspace services to JobSubmitter. HTTPErrCode:" + str(rspCode))

    return True

################################################################################
class FmeLog():
  def __init__(self, logStr):
    
    self.logStr = logStr
    arcpy.AddMessage("logstring:")
    arcpy.AddMessage(self.logStr)
  
  def process(self, writeIt, logErrs):
    if writeIt: self.writeFile()
    if logErrs: self.logErrs(logger)
    if "Translation was SUCCESSFUL" in self.logStr:
      arcpy.AddMessage("The FME translation was successful")
    else: #elif "Translation FAILED" in self.logStr:
      arcpy.AddMessage("The FME translation was not sucessful. Please check the logs.")
    
  def writeFile(self):
    if self.logStr[-1] == '\n': self.logStr = self.logStr[:-2]
    with open(self.logPath, "w") as fp:
      fp.write(self.logStr)
  def logErrs(self):  
    lines = self.logStr.split('\n')
    comps = [line.split('|') for line in lines]
    
    errs = [c[4] for c in comps if c[3].strip() == 'ERROR']
    if len(errs) > 0:
      arcpy.AddMessage("** FME ERRORS **")
      arcpy.AddMessage('\n'.join([err for err in errs]))
    
    warns = [c[4] for c in comps if c[3].strip() == 'WARN']
    if len(warns) > 0:
      arcpy.AddMessage("** FME WARNINGS **")
      arcpy.AddMessage('\n'.join([warn for warn in warns]))

################################################################################
class FmeCnxn():
  def __init__(self,  fmeSrvr, oraInsts, inst, username, bKeepIt, dbType='Ora'):
    self.srvr=fmeSrvr
    self.url="%s/fmerest/v3/namedconnections/connections" % (self.srvr.url)
    self.dbType = dbType

    self.inst=inst.lower()
    oraInst = [oi for oi in oraInsts if oi.name==inst]
    if len(oraInst) == 1:
      if self.dbType=='Ora':
        self.instance = "//%s:1521/%s" % (oraInst[0].server, oraInst[0].dbLink)
      else:
        self.instance = oraInst[0].server
    else:
      raise Exception("Instance %s not found in VMDD.ORACLE_INSTANCE_REGISTRY" % self.inst)
    
    self.name = "%s_%s" % (self.inst, self.username)
    if not self.persist:
      self.name = "%s_%s" % (self.name, datetime.now().strftime("%H%M%S"))

  def __str__(self):
    return self.name
  # def exists(self):
  #   return True if self.instance != None else False
  def check(self):
    arcpy.AddMessage("Checking " + self.name)
    rspCode, rsp = self.srvr.reqWithMeth("GET", 'json', self.url, None)
    if rspCode == 200:
      cnxns = [item['name'] for item in rsp['items']]
      return True if self.name in cnxns else False
    else:
      raise Exception("Could not get a good GET response. HTTP Code:" + str(rspCode))
  
  def create(self):
    arcpy.AddMessage("Creating " + self.name)
    rspCode, rsp = self.srvr.reqWithMeth("POST", 'json', self.url, self.mkDDict())
    if rspCode != 201: #201==Created
      arcpy.AddMessage("--Didn't create cnxn")
      raise Exception("Could not create a connection.\n  HTTP Code:%d\n  Reason:%s\n  Message:%s" % (rspCode, rsp['reason'], rsp['message']))
    else:
      arcpy.AddMessage("--Created cnxn")
  def mkDDict(self):
    if self.dbType=='Ora': #Oracle
      params = [{'name':'DATASET','value':self.instance}, \
        {'name':'USER_NAME','value':self.username}, \
        {'name':'PASSWORD','value':self.password}]
      return {'name':self.name,'sharable':False,'category':'DATABASE','type':'Oracle','parameters':params}
    else: #postgis
      params = [{'name':'PORT','value':'5432'}, \
        {'name':'SSLMODE','value':'prefer'}, \
        {'name':'HOST','value':''}, \
        {'name':'DATASET','value':self.instance.lower()}, \
        {'name':'USER_NAME','value':self.username.lower()}, \
        {'name':'PASSWORD','value':self.password}]
      return {'name':self.name,'sharable':False,'category':'DATABASE','type':'PostgreSQL','parameters':params}
  
  def delete(self):
    arcpy.AddMessage("Deleting " + self.name)
    rspCode, rsp = self.srvr.reqWithMeth("DELETE", 'json', self.url+'/'+self.name, None)
    if rspCode == 204:#204=='No Content'
      arcpy.AddMessage("--Deleted cnxn")
      return True
    else:
      arcpy.AddMessage('WARNING: did not delete cnxn:%s ErrCode:%s Reason:%s Message:%s' \
          % (self.name, str(rspCode), rsp['reason'], rsp['message']))
      return False      

    
if __name__ == '__main__':
  main()
