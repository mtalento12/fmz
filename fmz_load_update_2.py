import arcpy, os
from os import path

class fmzLoad():
    def __init__(self,year,quarter,conn):
        self.year        = year
        self.quarter     = quarter
        self.vsdlld       = conn
        #arcpy.AddMessage(self.conn)
        #self.vsdlld         = "Y:\\gein\\work\\mt12\\fmz\\dev\\connections\\"+self.conn
        self.location       = 'Y:\\gein\\work\\mt12\\fmz\\dev\\quarterly_updates'
        self.fmz_update     = self.vsdlld + "\\FORESTS.FMZ_UPDATE"
        self.fgdb           = self.location + "\\" + self.year + "\\" + self.quarter + "\\PROCESSING.gdb\\GDA"
        self.inputs         = "updates"
        self.source         = self.fgdb + "\\" +self.inputs
        
        self.gdb            = 'PROCESSING.gdb\\GDA'
        self.loc            =  self.location+'\\'+self.year+'\\'+self.quarter+'\\'+self.gdb+'\\'+ self.inputs
        self.field_mapping  = 'FMZ "FMZ" true true false 10 Text 0 0,First,#,'+self.loc+',FMZ,0,10;\
        FMZDIS "FMZDIS" true true false 8 Text 0 0,First,#,'+self.loc+',FMZDIS,0,8;\
        FMZ_NO "FMZ_NO" true true false 30 Text 0 0,First,#,'+self.loc+',FMZ_NO,0,30;\
        DESC1 "DESC1" true true false 254 Text 0 0,First,#,'+self.loc+',DESC1,0,254;\
        DESC2 "DESC2" true true false 254 Text 0 0,First,#,'+self.loc+',DESC2,0,254;\
        X_DESC "X_DESC" true true false 100 Text 0 0,First,#,'+self.loc+',X_DESC,0,100;\
        FORESTS_FM "FORESTS_FM" true true false 8 Double 8 38,First,#,'+self.loc+',FORESTS_FM,-1,-1;\
        ZDETFID "ZDETFID" true true false 8 Double 8 38,First,#,'+self.loc+',ZDETFID,-1,-1;\
        DETAILNO "DETAILNO" true true false 17 Text 0 0,First,#,'+self.loc+',DETAILNO,0,17;\
        LINEAR "LINEAR" true true false 18 Text 0 0,First,#,'+self.loc+',LINEAR,0,18;\
        FFG_POMA "FFG_POMA" true true false 45 Text 0 0,First,#,'+self.loc+',FFG_POMA,0,45;\
        FFG_SOMA "FFG_SOMA" true true false 15 Text 0 0,First,#,'+self.loc+',FFG_SOMA,0,15;\
        FFG_MOMA "FFG_MOMA" true true false 45 Text 0 0,First,#,'+self.loc+',FFG_MOMA,0,45;\
        FFG_LFP "FFG_LFP" true true false 31 Text 0 0,First,#,'+self.loc+',FFG_LFP,0,31;\
        FFG_STQ "FFG_STQ" true true false 13 Text 0 0,First,#,'+self.loc+',FFG_STQ,0,13;\
        TC_FIREZ "TC_FIREZ" true true false 17 Text 0 0,First,#,'+self.loc+',TC_FIREZ,0,17;\
        TC_APIARY "TC_APIARY" true true false 17 Text 0 0,First,#,'+self.loc+',TC_APIARY,0,17;\
        TC_CATCHM "TC_CATCHM" true true false 16 Text 0 0,First,#,'+self.loc+',TC_CATCHM,0,16;\
        RF_SOS "RF_SOS" true true false 15 Text 0 0,First,#,'+self.loc+',RF_SOS,0,15;\
        NE_BIOD "NE_BIOD" true true false 26 Text 0 0,First,#,'+self.loc+',NE_BIOD,0,26;\
        EVC_ONLY "EVC_ONLY" true true false 22 Text 0 0,First,#,'+self.loc+',EVC_ONLY,0,22;\
        EVC_OG "EVC_OG" true true false 30 Text 0 0,First,#,'+self.loc+',EVC_OG,0,30;\
        GUIDELINE "GUIDELINE" true true false 49 Text 0 0,First,#,'+self.loc+',GUIDELINE,0,49;\
        self.locAL_USE "self.locAL_USE" true true false 21 Text 0 0,First,#,'+self.loc+',self.locAL_USE,0,21;\
        SMS_RES "SMS_RES" true true false 101 Text 0 0,First,#,'+self.loc+',SMS_RES,0,101;\
        SMS_FLORA "SMS_FLORA" true true false 65 Text 0 0,First,#,'+self.loc+',SMS_FLORA,0,65;\
        SMS_REC "SMS_REC" true true false 45 Text 0 0,First,#,'+self.loc+',SMS_REC,0,45;\
        SMS_HISTO "SMS_HISTO" true true false 55 Text 0 0,First,#,'+self.loc+',SMS_HISTO,0,55;\
        RDLSCAPE "RDLSCAPE" true true false 25 Text 0 0,First,#,'+self.loc+',RDLSCAPE,0,25;\
        BLKCOMP "BLKCOMP" true true false 14 Text 0 0,First,#,'+self.loc+',BLKCOMP,0,14;\
        FMZTYPE "FMZTYPE" true true false 11 Text 0 0,First,#,'+self.loc+',FMZTYPE,0,11;\
        BLKTYP "BLKTYP" true true false 11 Text 0 0,First,#,'+self.loc+',BLKTYP,0,11;\
        BLK "BLK" true true false 6 Text 0 0,First,#,'+self.loc+',BLK,0,6;\
        FMZBLK "FMZBLK" true true false 10 Text 0 0,First,#,'+self.loc+',FMZBLK,0,10;\
        Z100FID "Z100FID" true true false 8 Double 8 38,First,#,'+self.loc+',Z100FID,-1,-1;\
        Z100UNIT "Z100UNIT" true true false 14 Text 0 0,First,#,'+self.loc+',Z100UNIT,0,14;\
        DETAILPT "DETAILPT" true true false 12 Text 0 0,First,#,'+self.loc+',DETAILPT,0,12;\
        AREA_HA "AREA_HA" true true false 8 Double 8 38,First,#,'+self.loc+',AREA_HA,-1,-1;\
        VERS_DATE "VERS_DATE" true true false 8 Date 0 0,First,#,'+self.loc+',VERS_DATE,-1,-1;\
        LEGAL_SET "LEGAL_SET" true true false 2 Short 0 5,First,#,'+self.loc+',LEGAL_SET,-1,-1;\
        SE_ANNO_CAD_DATA "SE_ANNO_CAD_DATA" true true false 0 Blob 0 0,First,#,'+self.loc+',SE_ANNO_CAD_DATA,-1,-1'

    def ex(self):
        
        #change workspace
        arcpy.env.workspace = self.vsdlld
        # arcpy.AddMessage(self.loc)
        arcpy.AddMessage(self.fmz_update)
        arcpy.AddMessage(self.vsdlld)
        

        if arcpy.Exists(self.loc):
            arcpy.AddMessage(self.source)
            arcpy.AddMessage("...Truncating FORESTS.FMZ_UPDATE FC")
            arcpy.TruncateTable_management(self.fmz_update)
            arcpy.AddMessage("...Appending new data to FORESTS.FMZ_UPDATE FCs")
            arcpy.Append_management(self.source, self.fmz_update, "NO_TEST", self.field_mapping, "")

        else:
            arcpy.AddMessage("...Source data inexistent. Load updates into GDA feature dataset")
