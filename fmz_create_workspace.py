#!/usr/bin/env python2.7
import arcpy,os
#import dev

class Workspace():
    def __init__(self,path):
        self.path     = path
        self.folder_y        = self.path

    #def execute(self,year,quarter):
        """The source code of the tool."""
    def ex(self):    
        arcpy.AddMessage("creating new workspaces")
        if arcpy.Exists(self.folder_y):
            pass
        else: # else create year and quarter folders
            arcpy.CreateFolder_management(self.path)

        New_Folder = self.path
        proce = "PROCESSING"
        gdb = New_Folder+"\\"+proce+".gdb"
        # Process: Create File GDB
        arcpy.CreateFileGDB_management(New_Folder, proce, "CURRENT")

        # Process: Create Feature Dataset
        arcpy.CreateFeatureDataset_management(gdb, "GDA", "4283")

        # Process: Create Feature Dataset VG
        arcpy.CreateFeatureDataset_management(gdb, "VG", "3111")

        return