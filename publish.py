import arcpy


class publishFMZ():
    def __init__(self,source_data,draft_sde,vsdlload):
        self.source_data         = source_data
        self.datenow             = "NOW()"
        self.target_sde          = vsdlload
        self.draft_sde           = draft_sde
        
        self.draft_data          = self.target_sde + "\\geoproc.forests." + self.source_data
        arcpy.AddMessage('Getting data from draft dataset: ' + self.draft_data)
        self.map_fields = 'FMZ_MAP \"FMZ_MAP\" true true false 10 Text 0 0 ,First,#,'+self.draft_sde+'\\geoproc.forests.FMZ_MAP100,FMZ_MAP,-1,-1;\
                    FMZDIS \"FMZDIS\" true true false 8 Text 0 0 ,First,#,'+self.draft_sde+'\\geoproc.forests.FMZ_MAP100,FMZDIS,-1,-1;\
                    MAP_NO \"MAP_NO\" true true false 30 Text 0 0 ,First,#,'+self.draft_sde+'\\geoproc.forests.FMZ_MAP100,MAP_NO,-1,-1;\
                    X_DESC \"X_DESC\" true true false 100 Text 0 0 ,First,#,'+self.draft_sde+'\\geoproc.forests.FMZ_MAP100,X_DESC,-1,-1;\
                    VERS_DATE \"VERS_DATE\" true true false 8 Text 0 0 ,First,#,'+self.draft_sde+'\\geoproc.forests.FMZ_MAP100,VERS_DATE,-1,-1;\
                    AREA_HA \"AREA_HA\" true true false 30 Text 0 0 ,First,#,'+self.draft_sde+'\\geoproc.forests.FMZ_MAP100,AREA_HA,-1,-1'
        
    def ex(self):
        if self.source_data == "fmz100":
            draft_data      = self.draft_sde+"\\geoproc.forests."+self.source_data
            fmz                 = self.target_sde+ "\\staging.forests."+self.source_data
            arcpy.AddMessage(fmz)
            field_mapping       = 'FMZ \"FMZ\" true true false 10 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,FMZ,-1,-1;\
                FMZDIS \"FMZDIS\" true true false 8 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,FMZDIS,-1,-1;\
                FMZ_NO \"FMZ_NO\" true true false 30 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,FMZ_NO,-1,-1;\
                DESC1 \"DESC1\" true true false 254 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,DESC1,-1,-1;\
                DESC2 \"DESC2\" true true false 254 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,DESC2,-1,-1;\
                X_DESC \"X_DESC\" true true false 100 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,X_DESC,-1,-1;\
                FORESTS_FM \"staging.forests_FM\" true true false 8 Double 4 15 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,FORESTS_FM,-1,-1;\
                ZDETFID \"ZDETFID\" true true false 8 Double 4 15 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,ZDETFID,-1,-1;\
                DETAILNO \"DETAILNO\" true true false 17 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,DETAILNO,-1,-1;\
                LINEAR \"LINEAR\" true true false 18 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,LINEAR,-1,-1;\
                FFG_POMA \"FFG_POMA\" true true false 45 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,FFG_POMA,-1,-1;\
                FFG_SOMA \"FFG_SOMA\" true true false 15 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,FFG_SOMA,-1,-1;\
                FFG_MOMA \"FFG_MOMA\" true true false 45 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,FFG_MOMA,-1,-1;\
                FFG_LFP \"FFG_LFP\" true true false 31 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,FFG_LFP,-1,-1;\
                FFG_STQ \"FFG_STQ\" true true false 13 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,FFG_STQ,-1,-1;\
                TC_FIREZ \"TC_FIREZ\" true true false 17 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,TC_FIREZ,-1,-1;\
                TC_APIARY \"TC_APIARY\" true true false 17 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,TC_APIARY,-1,-1;\
                TC_CATCHM \"TC_CATCHM\" true true false 16 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,TC_CATCHM,-1,-1;\
                RF_SOS \"RF_SOS\" true true false 15 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,RF_SOS,-1,-1;\
                NE_BIOD \"NE_BIOD\" true true false 26 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,NE_BIOD,-1,-1;\
                EVC_ONLY \"EVC_ONLY\" true true false 22 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,EVC_ONLY,-1,-1;\
                EVC_OG \"EVC_OG\" true true false 30 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,EVC_OG,-1,-1;\
                GUIDELINE \"GUIDELINE\" true true false 49 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,GUIDELINE,-1,-1;\
                LOCAL_USE \"LOCAL_USE\" true true false 21 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,LOCAL_USE,-1,-1;\
                SMS_RES \"SMS_RES\" true true false 101 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,SMS_RES,-1,-1;\
                SMS_FLORA \"SMS_FLORA\" true true false 65 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,SMS_FLORA,-1,-1;\
                SMS_REC \"SMS_REC\" true true false 45 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,SMS_REC,-1,-1;\
                SMS_HISTO \"SMS_HISTO\" true true false 55 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,SMS_HISTO,-1,-1;\
                RDLSCAPE \"RDLSCAPE\" true true false 25 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,RDLSCAPE,-1,-1;\
                BLKCOMP \"BLKCOMP\" true true false 14 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,BLKCOMP,-1,-1;\
                FMZTYPE \"FMZTYPE\" true true false 11 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,FMZTYPE,-1,-1;\
                BLKTYP \"BLKTYP\" true true false 11 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,BLKTYP,-1,-1;\
                BLK \"BLK\" true true false 6 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,BLK,-1,-1;\
                FMZBLK \"FMZBLK\" true true false 10 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,FMZBLK,-1,-1;\
                Z100FID \"Z100FID\" true true false 8 Double 4 15 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,Z100FID,-1,-1;\
                Z100UNIT \"Z100UNIT\" true true false 14 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,Z100UNIT,-1,-1;\
                DETAILPT \"DETAILPT\" true true false 12 Text 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,DETAILPT,-1,-1;\
                AREA_HA \"AREA_HA\" true true false 8 Double 4 15 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,AREA_HA,-1,-1;\
                VERS_DATE \"VERS_DATE\" true true false 36 Date 0 0 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,VERS_DATE,-1,-1;\
                LEGAL_SET \"LEGAL_SET\" true true false 2 Short 0 1 ,First,#,'+self.target_sde+'\\staging.forests.FMZ100,LEGAL_SET,-1,-1;\
                SHAPE_AREA \"SHAPE_AREA\" false false true 0 Double 0 0 ,First,#;\
                SHAPE_LEN \"SHAPE_LEN\" false false true 0 Double 0 0 ,First,#'
            #arcpy.env.workspace = "U:\\BusinessData\\fmz\\engine8_geoproc.forests.sde"
            
            arcpy.AddMessage("draft data:   " + draft_data)
            arcpy.AddMessage("...Loading FMZ100 into " + fmz)
            #arcpy.env.workspace = "U:\\BusinessData\\fmz\\vsdl_load_geoproc.forests.sde"
            arcpy.TruncateTable_management(fmz)
            arcpy.Append_management(draft_data, fmz, "NO_TEST", field_mapping, "")
            ora = arcpy.ArcSDESQLExecute(target_sde)
            sql = "UPDATE geoproc.forests.FMZ100 set VERS_DATE = (select current_date from dual)"
            sql_return = ora.execute(sql)
            sql_return = [sql_return]

            #arcpy.CalculateField_management(fmz, "VERS_DATE", self.datenow, "VB", "")
            arcpy.AddSpatialIndex_management(fmz, 500)

        else:
            
            draft_data      = self.draft_sde+"\\geoproc.forests.FMZ_MAP100"
            fmzmap     = self.target_sde+"\\geoproc.forests."+self.source_data
            arcpy.AddMessage(fmzmap)
            arcpy.AddMessage("...Loading FMZ_MAP into VSDLLOAD")
            arcpy.AddMessage("draft data: "+self.draft_data)
            arcpy.TruncateTable_management(fmzmap)
            arcpy.Append_management(draft_data, fmzmap, "NO_TEST", self.map_fields , "")
            ora = arcpy.ArcSDESQLExecute(self.target_sde)
            sql = "UPDATE geoproc.forests.FMZ_MAP100 set VERS_DATE = (select current_date from dual)"
            sql_return = ora.execute(sql)
            sql_return = [sql_return]
            #arcpy.CalculateField_management(fmzmap, "VERS_DATE", datenow, "VB", "")
            arcpy.AddSpatialIndex_management(fmzmap, 500)

        return
