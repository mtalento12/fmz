import arcpy


class fmzUpdate():
    def __init__(self,loc,conn):
        self.loc                 = loc
        self.test_sde            = conn
        arcpy.AddMessage('test sde : '+self.test_sde)
        self.sde_loc             = self.loc+'//connections'
        self.ws_vg           = self.loc + "//PROCESSING.gdb//VG"
        self.ws_gda              = self.loc +  "//PROCESSING.gdb//GDA"

        self.FMZ_OLD             =  self.test_sde + "//geoproc.forests.fmz100"

        self.QUARTERLY_UPDATE    = self.test_sde + "//geoproc.forests.fmz_update"

        self.fmz_old_fc          = self.loc + "//PROCESSING.gdb//VG//fmz_old"
        self.fmz_update_fc       = self.loc + "//PROCESSING.gdb//VG//fmz_update"

        self.fmz_old_Layer       = "fmz_old_7"
        self.fmz_update_Layer    = "fmz_update_7"
        self.new_fmz             = self.ws_gda + "//FMZ100"

        self.update              = self.loc + "//update.shp"
        self.dissolve            = self.ws_vg + "//dissolve"
        self.freq_fmz            = self.loc + "//FMZ_AREA.dbf"
        self.dis                 = "FMZ;FMZDIS;FMZ_NO;DESC1;DESC2;X_DESC;FORESTS_FM;ZDETFID;DETAILNO;LINEAR;FFG_POMA;FFG_SOMA;FFG_MOMA;FFG_LFP;FFG_STQ;TC_FIREZ;TC_APIARY;TC_CATCHM;RF_SOS;NE_BIOD;EVC_ONLY;EVC_OG;GUIDELINE;LOCAL_USE;SMS_RES;SMS_FLORA;SMS_REC;SMS_HISTO;RDLSCAPE;BLKCOMP;FMZTYPE;BLKTYP;BLK;FMZBLK;Z100FID;Z100UNIT;DETAILPT;LEGAL_SET"
            
    def ex(self):
        arcpy.AddMessage("...Importing Features into : "+self.ws_vg)

        if arcpy.Exists(self.fmz_old_fc):
            arcpy.Delete_management(self.fmz_old_fc)
            arcpy.CopyFeatures_management(self.FMZ_OLD, self.fmz_old_fc)

        else:
            arcpy.CopyFeatures_management(self.FMZ_OLD, self.fmz_old_fc)


        if arcpy.Exists(self.fmz_update_fc):
            arcpy.Delete_management(self.fmz_update_fc)
            arcpy.CopyFeatures_management(self.QUARTERLY_UPDATE, self.fmz_update_fc)

        else:
            arcpy.CopyFeatures_management(self.QUARTERLY_UPDATE, self.fmz_update_fc)

        arcpy.AddMessage("...Creating Layers")
        arcpy.MakeFeatureLayer_management(self.fmz_old_fc, self.fmz_old_Layer)

        # Process: Make Feature Layer (2)
        arcpy.MakeFeatureLayer_management(self.fmz_update_fc, self.fmz_update_Layer)

        # Process: Update

##        up = arcpy.Update_analysis(fmz_old_Layer, fmz_update_Layer, update, "BORDERS", "")

        if arcpy.Exists(self.update):
            arcpy.AddMessage("..." + self.update + " exists. Deleting " + self.update )
            arcpy.AddMessage("...Updating " + self.update )
            arcpy.Delete_management(self.update)
            up = arcpy.Update_analysis(self.fmz_old_Layer, self.fmz_update_Layer, self.update, "BORDERS", "")
            arcpy.AddMessage(up)
        else:
            arcpy.AddMessage("...Updating " +self.update)
            up = arcpy.Update_analysis(self.fmz_old_Layer, self.fmz_update_Layer, self.update, "BORDERS", "")
            arcpy.AddMessage(up)

        # Process: Dissolve
        if arcpy.Exists(self.dissolve):
            arcpy.AddMessage("..." + self.dissolve + " exists. Deleting " + self.dissolve)
            arcpy.AddMessage("...Dissolving " + self.update)
            arcpy.Delete_management(self.dissolve)
            arcpy.Dissolve_management(self.update, self.dissolve, self.dis, "", "SINGLE_PART", "DISSOLVE_LINES")

        else:
            arcpy.AddMessage("...Dissolving " + self.update)
            arcpy.Dissolve_management(self.update, self.dissolve, self.dis, "", "SINGLE_PART", "DISSOLVE_LINES")

        # Process: Add Field AREA_HA
        arcpy.AddMessage("...Adding Area Field")
        arcpy.AddField_management(self.dissolve, "AREA_HA", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")

        # Process: Add Field VERS_DATE
        arcpy.AddMessage("...Adding Version Date field")
        arcpy.AddField_management(self.dissolve, "VERS_DATE", "DATE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")

        # Process: Calculate Area (ha)
        arcpy.AddMessage("...Calculating the area in hectares")
        arcpy.CalculateField_management(self.dissolve, "AREA_HA", "!SHAPE.AREA@HECTARES!", "PYTHON_9.3", "")

        # Process: Frequency
        if arcpy.Exists(self.freq_fmz):
            arcpy.AddMessage("..." + self.freq_fmz + " exists. Deleting " + self.freq_fmz)
            arcpy.Delete_management(self.freq_fmz)
            arcpy.AddMessage("...Generating Area Summary Table")
            # To do: Drop Analysis Table
            arcpy.Frequency_analysis(self.dissolve, self.freq_fmz, "FMZ;FMZDIS", "AREA_HA")

        else:
            arcpy.AddMessage("...Generating Area Summary Table")
            arcpy.Frequency_analysis(self.dissolve, self.freq_fmz, "FMZ;FMZDIS", "AREA_HA")


        # Process: Copy Features
        if arcpy.Exists(self.new_fmz):
            arcpy.AddMessage("...Copy output to GDA")
            arcpy.CopyFeatures_management(self.dissolve, self.new_fmz, "", "0", "0", "0")
          

        else:
            arcpy.CopyFeatures_management(self.dissolve, self.new_fmz, "", "0", "0", "0")
    
        return
