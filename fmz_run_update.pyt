import arcpy,os,sys
import socket, imp

#import dev
class environment():
    def __init__(self):
      self.value = arcpy.GetSystemEnvironment("PATH")
      #arcpy.AddMessage(self.value)
      self.arcServer = self.value.find("ESRI")
      self.folder = "fmz"
      #arcpy.AddMessage(self.arcServer)

    def wspace(self):
      if self.arcServer < 0:
        #arcpy.AddMessage(self.arcServer)
        
        self.VSDL = "D://Users//Martin.Talento//projects//fmz//"+self.folder
        #gdbLocation = arcpy.env.scratchGDB
        arcpy.env.workspace = self.VSDL
        arcpy.AddMessage("GDB for processing located at: " + str(self.VSDL))
        arcpy.AddMessage("Scripts running on AWS")
        return self.VSDL
      else:
        #server = "//is-prd-gis-00.aaa.depi.vic.gov.au/GisData"
        self.servdir = os.getcwd()
        #arcpy.AddMessage("Server directory: "+self.servdir)
        self.wspace = arcpy.env.packageWorkspace
        arcpy.AddMessage("package workspace: "+self.wspace)
        #check if you  have rights to create a folder
        #join all paths to the package workspace
        self.VSDL = self.wspace+"//"+self.folder
        arcpy.AddMessage("Scripts running on Server: "+self.VSDL)
        return self.VSDL

class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Toolbox"
        self.alias = ""

        # List of tool classes associated with this toolbox
        self.tools = [Tool]


class Tool(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "fmzupdate"
        self.description = ""
        self.canRunInBackground = False

    def getParameterInfo(self):

        """Define parameter definitions"""
        action                    = arcpy.Parameter()
        action.name               = "function"
        action.displayName        = "function"
        action.direction          = "Input" # displays the parameter when published
        action.datatype           = "GPString"
        action.parameterType      = "Required"
        action.filter.list        = ['create_workspace', 'update_fmz', 'update_fmzmap','publish_fmz','publish_fmzmap']

        return [action]

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        try:
            self.function = parameters[0].valueAsText
            env = environment()
            self.dir = env.wspace()
            arcpy.AddMessage("directory : "+self.dir)
            self.conn_file = self.dir+"//geoproc_forests.sde"
            self.staging = self.dir+"//staging_forests.sde"
            arcpy.AddMessage('geoproc : '+self.conn_file)
            arcpy.AddMessage('staging : '+self.staging)
            # paths, processing env
              
            fmz_update  = "FORESTS.X_FMZ_UPDATE"
            
            # Datasets 
            fmz100             = "FORESTS.FMZ100"
            
            if self.function == 'create_workspace':
                
                import fmz_create_workspace
                imp.reload(fmz_create_workspace)
                self.create = fmz_create_workspace.Workspace(self.dir)
                self.create.ex()        

            elif self.function =='update_fmz': # forests vsdlload
               
                import fmz_update
                imp.reload(fmz_update)
                arcpy.AddMessage( "directory : "+ self.dir)
                self.upfmz = fmz_update.fmzUpdate(self.dir,self.conn_file)
                self.upfmz.ex()

            elif self.function == 'update_fmzmap':
                import fmzmap_update
                imp.reload(fmzmap_update)
                self.upmap = fmzmap_update.mapUpdate(self.dir,self.conn_file)
                self.upmap.ex()           
           
            elif self.function == 'publish_fmz':
               
                import FMEUtils as fm
                from FMEUtils import FmeParams, FmeServer
                self.repo = 'Business_apps'
                self.load_wrkspace = 'fmz100_load.fmw'

                arcpy.AddMessage('loading FMZ100 into geoproc')
                loadParams= fm.FmeParams([
                    ("SourceDataset_FILEGDB","$(FME_SHAREDRESOURCE_DATA)/VSDL/external_resources/PROCESSING.gdb"),
                    ("SourceDataset_POSTGIS","geoproc_forests"),
                    ("DestDataset_POSTGIS","geoproc_forests")
                ])
                server = fm.FmeServer(self.repo,self.load_wrkspace)
                server.execWkspc(loadParams.asDict())
                server.waitForJob()
                log = server.getLog()#%s.log" % (self.wrkpspace))
                arcpy.AddMessage(log)

                arcpy.AddMessage('syncing FMZ100 into staging')
                self.wrkpspace = 'fmz_sync.fmw'
                fmeParams = fm.FmeParams([
                    ("SourceDataset_POSTGIS","geoproc_forests"),
                    ("DestDataset_POSTGIS","staging_forests2"),
                    ("SourceDataset_POSTGIS_3","staging_forests2")
                ])
                server = fm.FmeServer(self.repo,self.wrkpspace)
                server.execWkspc(fmeParams.asDict())
                server.waitForJob()
                log = server.getLog()#%s.log" % (self.wrkpspace))
                arcpy.AddMessage(log)
            
            elif self.function == 'publish_fmzmap': 
                import load_fmz as l
                l.load('fmz_map100')

                import FMEUtils as fm
                from FMEUtils import FmeParams, FmeServer
                self.repo = 'Business_apps'
                self.wrkpspace = 'fmzmap_sync.fmw'
                fmeParams = fm.FmeParams([
                    ("SourceDataset_POSTGIS","geoproc_forests"),
                    ("DestDataset_POSTGIS","staging_forests2"),
                    ("SourceDataset_POSTGIS_3","staging_forests2")
                ])
                server = fm.FmeServer(self.repo,self.wrkpspace)
                server.execWkspc(fmeParams.asDict())
                server.waitForJob()
                log = server.getLog()#%s.log" % (self.wrkpspace))
                #arcpy.AddMessage(log)
               
        except arcpy.ExecuteError:
            arcpy.AddMessage(arcpy.GetMessages(2))
            arcpy.AddMessage("Error")

        return